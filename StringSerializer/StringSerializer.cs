﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;

namespace Serializer
{
    public class StringSerializer
    {
        public static string Serialize(object value)
        {
            var type = value.GetType();
            var result = new StringBuilder();

            foreach (var property in type.GetFields())
            {
                result.Append(property.Name);
                result.Append('=');
                result.Append(type.GetField(property.Name).GetValue(value));
                result.Append(',');
            }

            if (result.Length > 0) { result.Remove(result.Length - 1, 1); }

            return result.ToString();
        }

        public static object Deserialize(Type type, string value) {
            var result = Activator.CreateInstance(type);

            var fields = value.Split(',');
            foreach (var field in fields)
            {
                var fieldkeyValue = field.Split("=");
                var fieldKey = type.GetField(fieldkeyValue[0]);
                var fieldType = fieldKey.FieldType;
                fieldKey.SetValue(result, Convert.ChangeType(fieldkeyValue[1], fieldType));
            }

            return result;
        }
    }
}
