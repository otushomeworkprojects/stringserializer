﻿using Serializer;
using System.Diagnostics;
using System.Text.Json;

var f = new F();
var str = new StringSerializer();
f = f.Get();
var stopwatch = new Stopwatch();
stopwatch.Start();
for (int i = 0; i < 1000; i++)
    StringSerializer.Serialize(f);
stopwatch.Stop();
Console.WriteLine($"Сериализация в строку: {stopwatch.ElapsedMilliseconds} mls");
stopwatch.Reset();

stopwatch.Start();
Console.WriteLine($"Вывод текста в консоль...");
stopwatch.Stop();
Console.WriteLine($"Ушло времени: {stopwatch.ElapsedMilliseconds} mls");
stopwatch.Reset();

var json = "";
stopwatch.Start();
for (int i = 0; i < 1000; i++)
    json = JsonSerializer.Serialize(f);
stopwatch.Stop();
Console.WriteLine($"Сериализация в JSON: {stopwatch.ElapsedMilliseconds} mls");
stopwatch.Reset();

stopwatch.Start();
for (int i = 0; i < 1000; i++)
    StringSerializer.Deserialize(typeof(F), "i1=1,i2=2,i3=3,i4=4,i5=5");
stopwatch.Stop();
Console.WriteLine($"Десериализация из строки: {stopwatch.ElapsedMilliseconds} mls");

stopwatch.Start();
for (int i = 0; i < 1000; i++)
    JsonSerializer.Deserialize<F>(json);
stopwatch.Stop();
Console.WriteLine($"Десериализация из JSON: {stopwatch.ElapsedMilliseconds} mls");
stopwatch.Reset();

Console.ReadLine();